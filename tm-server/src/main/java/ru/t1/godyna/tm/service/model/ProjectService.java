package ru.t1.godyna.tm.service.model;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.godyna.tm.api.repository.model.IProjectRepository;
import ru.t1.godyna.tm.api.repository.model.IUserRepository;
import ru.t1.godyna.tm.api.service.IProjectService;
import ru.t1.godyna.tm.enumerated.Sort;
import ru.t1.godyna.tm.enumerated.Status;
import ru.t1.godyna.tm.exception.entity.ProjectNotFoundException;
import ru.t1.godyna.tm.exception.entity.StatusEmptyException;
import ru.t1.godyna.tm.exception.field.DescriptionEmptyException;
import ru.t1.godyna.tm.exception.field.IdEmptyException;
import ru.t1.godyna.tm.exception.field.NameEmptyException;
import ru.t1.godyna.tm.exception.field.UserIdEmptyException;
import ru.t1.godyna.tm.model.Project;

import java.util.Collection;
import java.util.List;

@Service
@NoArgsConstructor
public class ProjectService implements IProjectService {

    @NotNull
    @Autowired
    private IProjectRepository projectRepository;

    @NotNull
    @Autowired
    private IUserRepository userRepository;

    @NotNull
    @Override
    @Transactional
    public Project add(@Nullable final Project model) {
        if (model == null) throw new ProjectNotFoundException();
        projectRepository.saveAndFlush(model);
        return model;
    }

    @NotNull
    @Override
    @Transactional
    public Project add(@Nullable final String userId, @Nullable final Project model) {
        if (model == null) throw new ProjectNotFoundException();
        model.setUser(userRepository.findById(userId).orElse(null));
        projectRepository.saveAndFlush(model);
        return model;
    }

    @NotNull
    @Override
    @Transactional
    public Collection<Project> add(@NotNull final Collection<Project> models) {
        if (models == null) throw new ProjectNotFoundException();
        return projectRepository.saveAll(models);
    }

    @NotNull
    @Override
    @Transactional
    public Project changeProjectStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (status == null) throw new StatusEmptyException();
        @Nullable final Project project = projectRepository.findByUserIdAndId(userId, id);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(status);
        return update(project);
    }

    @Override
    @Transactional
    public void clear() {
        projectRepository.deleteAll();
    }

    @Override
    @Transactional
    public void clear(@Nullable final String userId) {
        projectRepository.deleteByUserId(userId);
    }

    @NotNull
    @Override
    @Transactional
    public Project create(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @NotNull Project project = new Project();
        project.setName(name);
        return add(userId, project);
    }

    @NotNull
    @Override
    @Transactional
    public Project create(@Nullable final String userId, @Nullable final String name, @Nullable final String description) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @NotNull Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        return add(userId, project);
    }

    @Override
    public boolean existsById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return projectRepository.existsById(id);
    }

    @Override
    public boolean existsById(@Nullable final String userId, @Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return projectRepository.existsByUserIdAndId(userId, id);
    }

    @Nullable
    @Override
    public List<Project> findAll() {
        return projectRepository.findAll();
    }

    @Nullable
    @Override
    public List<Project> findAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return projectRepository.findAllByUserId(userId);
    }

    @Nullable
    @Override
    public List<Project> findAll(@Nullable final String userId, @Nullable final Sort sort) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if(sort == null) return projectRepository.findAllByUserId(userId);
        return projectRepository.findAllByUserIdWithSort(userId, getSortType(sort));
    }

    @Nullable
    @Override
    public Project findOneById(@Nullable final String userId, @Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return projectRepository.findByUserIdAndId(userId, id);
    }

    @Override
    public long getSize(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return projectRepository.countByUserId(userId);
    }

    @NotNull
    @Override
    @Transactional
    public Project remove(@Nullable final Project model) {
        if (model == null) throw new ProjectNotFoundException();
        projectRepository.delete(model);
        return model;
    }

    @NotNull
    @Override
    @Transactional
    public Project remove(@Nullable final String userId, @Nullable final Project model) {
        if (model == null) throw new ProjectNotFoundException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        projectRepository.deleteByUserIdAndId(userId, model.getId());
        return model;
    }

    @Override
    @Transactional
    public void removeAll(@Nullable final Collection<Project> collection) {
        if (collection == null) throw new ProjectNotFoundException();
        projectRepository.deleteAll(collection);
    }

    @NotNull
    @Override
    @Transactional
    public Project removeById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable Project project = projectRepository.findByUserIdAndId(userId, id);
        projectRepository.deleteByUserIdAndId(userId, id);
        return project;
    }

    @NotNull
    @Override
    @Transactional
    public Collection<Project> set(@NotNull final Collection<Project> collections) {
        if (collections == null) throw new ProjectNotFoundException();
        clear();
        add(collections);
        return collections;
    }

    @NotNull
    @Override
    @Transactional
    public Project update(@NotNull final Project model) {
        if (model == null) throw new ProjectNotFoundException();
        projectRepository.saveAndFlush(model);
        return model;
    }

    @NotNull
    @Override
    @Transactional
    public Project updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @Nullable final Project project = findOneById(userId, id);
        if (project == null) throw new ProjectNotFoundException();
        project.setName(name);
        project.setDescription(description);
        projectRepository.saveAndFlush(project);
        return project;
    }

    @NotNull
    private String getSortType(@NotNull Sort sort) {
        if (sort == Sort.BY_CREATED) return "name";
        else if (sort == Sort.BY_STATUS) return "status";
        else return "created";
    }

}
