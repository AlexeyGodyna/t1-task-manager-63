package ru.godyna.tm.repository;

import lombok.NonNull;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.godyna.tm.model.Project;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

public class ProjectRepository {

    private static final ProjectRepository INSTANCE = new ProjectRepository();

    public static ProjectRepository getInstance() {
        return INSTANCE;
    }

    @NonNull
    private Map<String, Project> projects = new LinkedHashMap<>();

    {
        add(new Project("One"));
        add(new Project("Two"));
        add(new Project("Tree"));
        add(new Project("Four"));
        add(new Project("Five"));
    }

    public void create() {
        add(new Project("New Project " + System.currentTimeMillis()));
    }

    public void add(@NonNull final Project project) {
        projects.put(project.getId(), project);
    }

    public void save(@NonNull final Project project) {
        projects.put(project.getId(), project);
    }

    @Nullable
    public Collection<Project> findAll() {
        return projects.values();
    }

    @Nullable
    public Project findById(@NotNull final String id) {
        return projects.get(id);
    }

    public void removeById(@NotNull final String id) {
        projects.remove(id);
    }

}
