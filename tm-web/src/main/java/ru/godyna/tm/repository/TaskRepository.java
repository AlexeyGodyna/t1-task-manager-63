package ru.godyna.tm.repository;

import lombok.NonNull;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.godyna.tm.model.Task;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

public class TaskRepository {

    private static final TaskRepository INSTANCE = new TaskRepository();

    public static TaskRepository getInstance() {
        return INSTANCE;
    }

    @NonNull
    private Map<String, Task> tasks = new LinkedHashMap<>();

    {
        add(new Task("One"));
        add(new Task("Two"));
        add(new Task("Tree"));
        add(new Task("Four"));
        add(new Task("Five"));
    }

    public void create() {
        add(new Task("New Task " + System.currentTimeMillis()));
    }

    public void add(@NonNull final Task task) {
        tasks.put(task.getId(), task);
    }

    public void save(@NonNull final Task task) {
        tasks.put(task.getId(), task);
    }

    @Nullable
    public Collection<Task> findAll() {
        return tasks.values();
    }

    @Nullable
    public Task findById(@NotNull final String id) {
        return tasks.get(id);
    }

    public void removeById(@NotNull final String id) {
        tasks.remove(id);
    }

}
