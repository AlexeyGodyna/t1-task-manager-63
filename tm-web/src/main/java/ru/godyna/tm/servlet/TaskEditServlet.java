package ru.godyna.tm.servlet;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.godyna.tm.enumerated.Status;
import ru.godyna.tm.model.Task;
import ru.godyna.tm.repository.ProjectRepository;
import ru.godyna.tm.repository.TaskRepository;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.SimpleDateFormat;

@WebServlet("/task/edit/*")
public class TaskEditServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        @NotNull final String id = req.getParameter("id");
        @Nullable final Task task = TaskRepository.getInstance().findById(id);
        req.setAttribute("task", task);
        req.setAttribute("statuses", Status.values());
        req.setAttribute("projects", ProjectRepository.getInstance().findAll());
        req.getRequestDispatcher("/WEB-INF/views/task-edit.jsp").forward(req, resp);
    }

    @Override
    @SneakyThrows
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        @NotNull final String id = req.getParameter("id");
        @NotNull final String projectId = req.getParameter("projectId");
        @NotNull final String name = req.getParameter("name");
        @NotNull final String description = req.getParameter("description");
        @NotNull final String statusValue = req.getParameter("status");
        @NotNull final Status status = Status.valueOf(statusValue);
        @NotNull final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        @Nullable final String dateStartValue = req.getParameter("dataStart");
        @Nullable final String dateFinishValue = req.getParameter("dataFinish");

        @NotNull final Task task = new Task();
        task.setId(id);
        task.setProjectId(projectId);
        task.setName(name);
        task.setDescription(description);
        task.setStatus(status);
        if (!dateStartValue.isEmpty()) task.setDateStart(simpleDateFormat.parse(dateStartValue));
        else task.setDateStart(null);
        if (!dateFinishValue.isEmpty()) task.setDateFinish(simpleDateFormat.parse(dateFinishValue));
        else task.setDateFinish(null);

        TaskRepository.getInstance().save(task);
        resp.sendRedirect("/tasks");
    }

}
